//
//  CalculatorViewModel.swift
//  CodeCoverageDemo
//
//  Created by khanh.vo on 7/29/21.
//

import Foundation

public class CalculatorViewModel {
    public init() { }
    
    public func add<T>(a: T, b: T) -> T where T: Comparable, T: SignedNumeric {
        return a + b
    }
    
    public func subtract<T>(a: T, b: T) -> T where T: Comparable, T: SignedNumeric {
        return a - b
    }
    
    public func multiply<T>(a: T, b: T) -> T where T: Comparable, T: SignedNumeric {
        return a * b
    }
    
    public func divide<T>(a: T, b: T) throws -> T where T: FloatingPoint {
        guard b != 0 else {
            throw NSError(domain: "vn.tiki.calculator", code: 1000, userInfo: ["msg": "b must be different 0"])
        }
        return a / b
    }
}
