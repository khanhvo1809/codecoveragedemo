//
//  CodeCoverageDemoTests.swift
//  CodeCoverageDemoTests
//
//  Created by khanh.vo on 7/29/21.
//

import XCTest
@testable import CodeCoverageDemo
import ShapeModule

class CodeCoverageDemoTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCalculatorViewModel() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let calculator = CalculatorViewModel()
        
        let add = calculator.add(a: 1.5, b: 2)
        XCTAssert(add == 3.5, "add func issue")
        
        let subtract = calculator.subtract(a: 1.5, b: 0.2)
        XCTAssert(subtract == 1.3, "subtract func issue")
        
        let multiply = calculator.multiply(a: 1.5, b: 2)
        XCTAssert(multiply == 3, "multiply func issue")
        
        let divide = try? calculator.divide(a: 3, b: 2)
        XCTAssert(divide == 1.5, "divide func issue")

        do {
            let divide = try calculator.divide(a: 1.5, b: 0)
        }
        catch let err {
            let error = err as? NSError
            XCTAssert(error != nil, "error must be non nil")
            XCTAssert(error?.domain == "vn.tiki.calculator", "wrong domain")
            XCTAssert(error?.code == 1000, "wrong code")
            
            let msg = error?.userInfo["msg"] as? String
            XCTAssert(msg == "b must be different 0", "wrong error msg")
        }
    }

    func testShapeModule() throws {
        let circle = Circle()
        XCTAssert(circle.numberOfSides == 0, "Circle does not have any sides")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
